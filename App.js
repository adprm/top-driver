import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Routes from './src/Screen/Routes'
import Home from './src/Screen/Home'
import Kendaraan from './src/Screen/Kendaraan'
import Absensi from './src/Screen/Absensi'
import Detail from './src/Screen/Home/Detail'
import Profile from './src/Screen/Profile'
import Terima from './src/Screen/Home/Terima'
import Maps from './src/Screen/Home/Maps'
import Callc from './src/Screen/Home/Callc'

const App = () => {
  return (
    <Routes/>
  );
};
export default App;
