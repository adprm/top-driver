import React, { useState } from 'react';
import { FlatList, Image, StatusBar, StyleSheet, Text, View, Modal,TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import StepIndicator from 'react-native-step-indicator';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';
import StepStyle from '../../Style/StepStyle';

const DATA = [
    {
        id : 1,
        no : '#123456789',
        tgl : '20 Juli 2020',
        nama : 'Bapak Havid',
        volume : 10,
        barang : 'Besi',
        penjemputan : 'Gudang Bogor',
        Penerimaan : 'Gudang Jakarta'
    },
    {
        id : 2,
        no : '#123456789',
        tgl : '20 Juli 2020',
        nama : 'Bapak Havid',
        volume : 10,
        barang : 'Besi',
        penjemputan : 'Gudang Jakarta',
        Penerimaan : 'Gudang Bandung'
    },
    {
        id : 3,
        no : '#123456789',
        tgl : '20 Juli 2020',
        nama : 'Bapak Havid',
        volume : 10,
        barang : 'Besi',
        penjemputan : 'Gudang Bogor',
        Penerimaan : 'Gudang Lampung'
    }
]

const Pending = ({navigation}) => {
    const [curent, setCurent] = useState(1);
    const [modalVisible, setModalVisible] = useState(false);

    const renderModal = () => {
        return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            }}>
            <View style={styleses.centeredView}>
            <View style={styleses.modalView}>
                <Text style={[styleses.modalText,{color:colors.green}]}>Menerima Tugas</Text>
                <Text style={{marginVertical:20}}>Apakah anda yakin menerima tugas ini ?</Text>
                <View style={{flexDirection:'row',margin:20}}>
                <Button title="Kembali" type="outline" buttonStyle={{marginRight:10,width:130}} titleStyle={{color:'black'}}
                onPress={() => {
                    setModalVisible(!modalVisible);
                }}/>
                <Button title="Terima" buttonStyle={{width:130,backgroundColor:colors.green}}/>
                </View>
            </View>
            </View>
        </Modal>
        )
    }

    const Item = ({data}) => {
        const penjemputan = () => {
            return(
                <View style={{paddingLeft:20}}>
                <Text style={{color:colors.green}}>Lokasi Penjemputan</Text>
                <Text>{data.penjemputan}</Text>
                </View>
            )
        }
    
        const penerima = () => {
            return(
                <View>
                <Text style={{color:colors.darkblue}}>Lokasi Penerima</Text>
                <Text>{data.Penerimaan}</Text>
                </View>
            )
        }
    
        const labels = [penjemputan(),penerima()];
        
        return (
        <View style={{marginHorizontal:20,marginVertical:10}}>
        <View style={{backgroundColor:colors.white,
            borderRadius: 10,
            elevation: 5,
            zIndex: 1,
            width:'100%',
            height: 330
            }}>

        <View style={{height:30,backgroundColor:colors.darkblue,opacity: 0.1,borderTopLeftRadius:10,borderTopRightRadius:10}} />
        <Text style={{color:colors.darkblue,alignSelf:'center',position:'absolute',paddingTop:3}}>No Surat Jalam : {data.no}</Text>
        <View style={{flexDirection:'column',padding:10}}>
        {/* data */}
        <View style={{flexDirection: 'row',justifyContent: 'space-between',marginVertical:5}}>
        <Text style={{flex:2}}>Tanggal</Text>
        <Text style={{flex:1}}>:</Text>
        <Text style={{flex:1}}>{data.tgl}</Text>
        </View>
        <View style={{flexDirection: 'row',justifyContent: 'space-between',marginVertical:5}}>
        <Text style={{flex:2}}>Nama Pelanggan</Text>
        <Text style={{flex:1}}>:</Text>
        <Text style={{flex:1}}>{data.nama}</Text>
        </View>
        <View style={{flexDirection: 'row',justifyContent: 'space-between',marginVertical:5}}>
        <Text style={{flex:2}}>Volume yang diterima</Text>
        <Text style={{flex:1}}>:</Text>
        <Text style={{flex:1}}>{data.volume}</Text>
        </View>
        <View style={{flexDirection: 'row',justifyContent: 'space-between',marginVertical:5}}>
        <Text style={{flex:2}}>Nama Barang</Text>
        <Text style={{flex:1}}>:</Text>
        <Text style={{flex:1}}>{data.barang}</Text>
        </View>
        {/* data */}
        </View>

        <View style={{height:90,marginHorizontal:10,marginBottom:10}}>
        <StepIndicator
         customStyles={StepStyle}
         currentPosition={curent}
         labels={labels}
         stepCount={2}
         direction={'vertical'}
        />
        </View>

        <Button 
        title="Ambil Tugas"
        buttonStyle={{margin:10,backgroundColor:colors.darkblue}}
        onPress={() => {
            setModalVisible(true);
          }}
        />
        </View>
        </View>
        );
    }

    const renderItem = ({ item }) => (
        <Item data={item} />
      );

    return (
        <View style={styleses.wrap}>
            <StatusBar barStyle="dark-content"/>
            <View style={{flex:1,backgroundColor:colors.white,paddingTop:15}}>
            
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />

            </View>
            {renderModal()}
        </View>
    )
}

export default Pending