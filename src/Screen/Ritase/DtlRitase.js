import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, Image, ScrollView } from 'react-native'
import { Button,Input } from 'react-native-elements';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';

const DtlRitase = () => {

    const labelred = (text) => {
        return (
            <View style={{flexDirection:'row'}}>
                <Text>{text}</Text>
                <Text style={{color:'red'}}>*</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={[styleses.wrap,{backgroundColor:colors.white}]}>
        <ScrollView>
           <View style={{flex:1,margin:20,flexDirection:'column'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <Image source={require('../../Assets/top-driver.png')} style={{width:48,height:48}}/>
                <Image source={require('../../Assets/wan.png')} style={{width:48,height:48}}/>
            </View>
            <View
            style={{
                borderBottomColor: colors.black,
                borderBottomWidth: 1,
            }}
            />
            <View style={{flexDirection:'column',alignItems:'center',marginVertical:10}}>
                <Image source={require('../../Assets/img-notfound.png')} style={{width:60,height:60,marginBottom:10}}/>
                <Text style={{marginVertical:10}}>Logo PT</Text>
                <Text style={{marginBottom:10}}>Jln. Raya Bogor,Bogor Tengah,Kota Bogor,Jawa Barat</Text>
            </View>
            <View
            style={{
                borderBottomColor: colors.black,
                borderBottomWidth: 1,
            }}
            />
            {/* isi */}
            <View style={{flexDirection:'column',marginVertical:20}}>
            <Text style={{alignSelf:'center',marginBottom:10}}>Surat Jalan</Text>
            {/* sj 1 */}
            <View style={{marginBottom:20}}>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>No. Surat Jalan</Text>
                <Text style={{flex:1}}>:</Text>
                <Text style={{flex:2,textAlign:'right'}}>#1234567890</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Nama Pelanggan</Text>
                <Text style={{flex:1}}>:</Text>
                <Text style={{flex:2,textAlign:'right'}}>Yth. Bapak Kevin Sudrajat</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Alamat Kirim</Text>
                <Text style={{flex:1}}>:</Text>
                <Text style={{flex:2,textAlign:'right'}}>Pabrik Sukabumi</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Tanggal Kirim</Text>
                <Text style={{flex:1}}>:</Text>
                <Text style={{flex:2,textAlign:'right'}}>23 Juli 2020</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Jam Kirim</Text>
                <Text style={{flex:1}}>:</Text>
                <Text style={{flex:2,textAlign:'right'}}>07:30</Text>
            </View>
            </View>
            {/* sj 1 */}
            {/* sj 2 */}
                <View>
                    <Input
                    label='Nama Supir'
                    value='Ade Rohayat Satria'
                    labelStyle={[styleses.inputLabel,{color:colors.black}]}
                    inputStyle={styleses.input}
                    inputContainerStyle={styles.inputContain}
                    />
                        <View style={{flexDirection:'row'}}>
                        <View style={styleses.wrap}>
                        <Input
                        label={labelred('No. Polisi ')}
                        value='F 123 AB'
                        labelStyle={styleses.inputLabel}
                        inputStyle={styleses.input}
                        inputContainerStyle={styles.inputContain}
                        />
                        </View>
                        <View style={styleses.wrap}>
                        <Input
                        label={labelred('Jenis Armada ')}
                        value='F 123 AB'
                        labelStyle={styleses.inputLabel}
                        inputStyle={styleses.input}
                        inputContainerStyle={styles.inputContain}
                        />
                        </View>
                        </View>
                    
                    <Input
                    label={labelred('Nama Barang ')}
                    value='Barang'
                    labelStyle={styleses.inputLabel}
                    inputStyle={styleses.input}
                    inputContainerStyle={styles.inputContain}
                    />
                        <View style={{flexDirection:'row'}}>
                        <View style={styleses.wrap}>
                        <Input
                        label={labelred('Volume ')}
                        value='volume'
                        labelStyle={styleses.inputLabel}
                        inputStyle={styleses.input}
                        inputContainerStyle={styles.inputContain}
                        />
                        </View>
                        <View style={styleses.wrap}>
                        <Input
                        label={labelred('Satuan ')}
                        value='satuan'
                        labelStyle={styleses.inputLabel}
                        inputStyle={styleses.input}
                        inputContainerStyle={styles.inputContain}
                        />
                        </View>
                        </View>
                    
                    <Input
                    label={labelred('Keterangan ')}
                    value='keterangan barang'
                    labelStyle={styleses.inputLabel}
                    inputStyle={styleses.input}
                    inputContainerStyle={styles.inputContain}
                    />
                
                    <View style={{flexDirection:'row'}}>
                        <Text style={{margin:10}}>Foto Surat Jalan: </Text>
                        <Button title='Lihat Foto' buttonStyle={{backgroundColor:colors.darkblue}} />
                    </View>

                </View>
                {/* sj 2 */}
            </View>
            {/* isi */}

           </View>
        </ScrollView>
        </SafeAreaView>
    )
}

export default DtlRitase

const styles = StyleSheet.create({
    inputContain:{
        borderBottomColor:'black'
    }
})
