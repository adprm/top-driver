import React,{useState,useEffect,useRef } from 'react'
import { FlatList, Image, StatusBar, StyleSheet, Text, View, Modal,TouchableOpacity, ToastAndroid } from 'react-native';
import { Button,Input } from 'react-native-elements';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';
import { RNCamera } from 'react-native-camera';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Feather from 'react-native-vector-icons/Feather';

const Terima = () => {

    const camera = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [editable, setEditable] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)
    const [token, setToken] = useState('')

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        // const option = {quality: 0.5, base64 : true};
        // if(camera){
        //     const data = await camera.current.takePictureAsync(option);
        //     setPhoto(data)
            setIsVisible(false)
            ToastAndroid.show("Sukses Mengambil Foto!", ToastAndroid.SHORT)
        // }
    }


    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1, }}
                        type={type}
                        ref={camera}
                    >
                        <View style={{
                             padding:25,
                             width:80,
                             height:80,
                             borderRadius:80,
                             backgroundColor:'black'
                        }}>
                            <TouchableOpacity style={{}} onPress={() => toggleCamera()}>
                                <MaterialCommunityIcons name={'rotate-3d-variant'} size={30} color={colors.white}/>
                            </TouchableOpacity>
                        </View>
                          <View style={{
                              width:300,
                              height:'75%',
                            //   borderRadius:80,
                            //   borderColor:'black',
                            //   borderWidth:3,
                            //   borderColor: colors.black,
                              marginHorizontal: 50
                        }} />
                        <View style={{
                            padding:25,
                            width:80,
                            height:80,
                            borderRadius:80,
                            backgroundColor:'black',
                            alignSelf:'center',
                        }} >
                            <TouchableOpacity style={{}} onPress={() => takePicture()}>
                                <Feather name={'camera'} size={30} color={colors.white} />
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={[styleses.wrap,{backgroundColor:colors.white}]}>
            <View style={{margin:20,justifyContent:'space-between',flex:1,}}>
                {/* form */}
                <View style={{paddingTop:50}}>
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                    <Text style={{marginTop:20}}>Volume Di Terima </Text>
                    <Text style={{marginTop:20}}>:</Text>
                    <View style={{
                    width:150,
                    borderRadius:10,
                    height:50,
                    marginVertical:10,
                    borderColor:colors.black,
                    borderWidth:1}}>
                    <Input
                    value={'5'}
                    inputContainerStyle={{borderBottomWidth:0}}
                    style={{padding:10}}
                    inputStyle={{fontSize:15}}
                    />
                    </View>
                </View>
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                    <Text style={{marginTop:20}}>Penerima Surat Jalan </Text>
                    <Text style={{marginTop:20}}>:</Text>
                    <View style={{
                    width:150,
                    borderRadius:10,
                    height:50,
                    marginVertical:10,
                    borderColor:colors.black,
                    borderWidth:1}}>
                    <Input
                    value={'Adi Santoso'}
                    inputContainerStyle={{borderBottomWidth:0}}
                    style={{padding:10}}
                    inputStyle={{fontSize:15}}
                    />
                    </View>
                </View>
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                    <Text style={{marginTop:10}}>Foto Surat Jalan Di Terima </Text>
                    <Text style={{marginTop:10}}>:</Text>
                    <Button title='Ambil Foto' onPress={() => setIsVisible(true)} buttonStyle={{backgroundColor:colors.darkblue}} />
                </View>
                </View>
                {/* form */}
                <Text style={{color:'red',alignSelf:'center'}}> Segera Kemalikan Surat Jalan Ke Admin </Text>
                <Button title='Submit' buttonStyle={{backgroundColor:colors.darkblue}} />
            </View>
            {renderCamera()}
        </View>
    )
}

export default Terima

const styles = StyleSheet.create({})
