import React,{useState} from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView, StatusBar, Modal, TouchableOpacity } from 'react-native'
import { Button,Input } from 'react-native-elements';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Maps = ({navigation}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [modalVisible1, setModalVisible1] = useState(false);


    const renderModal = () => {
        return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            }}>
            <View style={styleses.centeredView}>
            <View style={{
                 margin: 20,
                 backgroundColor: "white",
                 borderRadius: 10,
                 padding: 20,
                //  alignItems: "center",
                 shadowColor: "#000",
                 shadowOffset: {
                   width: 0,
                   height: 2
                 },
                 shadowOpacity: 0.25,
                 shadowRadius: 3.84,
                 elevation: 5,
                 height:200,
                 width:330
            }}>
                <View style={{flexDirection:'row'}}>
                <TouchableOpacity 
                    onPress={() => {
                        setModalVisible(!modalVisible);
                    }}
                >
                    <AntDesign name='arrowleft' size={30} color={colors.black} style={{}} />
                </TouchableOpacity>
                <Text style={{ 
                // marginBottom: 15,
                marginLeft:'20%',
                // textAlign: "center",
                fontSize:21,
                fontWeight:'bold',
                color:colors.green
                }}>Tiba Di Tujuan</Text>
                </View>
                <View style={{flexDirection:'column'}}>
                <Text style={{marginVertical:30,alignSelf:'center'}}>Apakah anda yakin sudah tiba di tujuan ?</Text>
                </View>
                <View style={{flexDirection:'row',margin:20,alignSelf:'center'}}>
                <Button title="Reject" type="outline" buttonStyle={{marginRight:10,width:120}} titleStyle={{color:'red'}}
                onPress={() => {
                    setModalVisible(!modalVisible);
                    setModalVisible1(true);
                }}/>
                <Button title="Terima" buttonStyle={{width:120,backgroundColor:colors.green}} 
                onPress={() => {navigation.navigate('Terima'),setModalVisible(!modalVisible)}}/>
                </View>
            </View>
            </View>
        </Modal>
        )
    }

    const renderModalReject = () => {
        return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible1}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            }}>
            <View style={styleses.centeredView}>
            <View style={[styleses.modalView,{height:250}]}>
                <Text style={[styleses.modalText,{color:'red',alignSelf:'center'}]}>Alasan Reject</Text>
                <Input
                        label='Masukan Alasan :'
                        labelStyle={styleses.inputLabel}
                        inputStyle={styleses.input}
                        inputContainerStyle={styles.inputContain}
                        />

                <View style={{flexDirection:'row',margin:20}}>
                <Button title="Kembali" type="outline" buttonStyle={{marginRight:10,width:130}} titleStyle={{color:colors.black}}
                onPress={() => {
                    setModalVisible1(!modalVisible1);
                }}/>
                <Button title="Submit" buttonStyle={{width:130,backgroundColor:'red'}} 
                onPress={() => {setModalVisible1(!setModalVisible1)}}/>
                </View>
            </View>
            </View>
        </Modal>
        )
    }

    return (
        <SafeAreaView style={[styleses.wrap,{backgroundColor:colors.white}]}>
            <StatusBar translucent backgroundColor="transparent" barStyle="dark-content"/>
            {/* <Image source={require('../../Assets/snk.jpg')} style={{width:'100%',height:'80%'}} /> */}
            <View style={{
                position:'absolute',
                flexDirection:'row',
                backgroundColor:colors.white,
                width:'90%',
                height:55,
                borderRadius:10,
                alignSelf:'center',
                marginTop:50,
                elevation: 10,
                zIndex: 1
                }}>
                <View style={{alignSelf:'center',flexDirection:'row',marginHorizontal:'35%'}}>
                    <Text style={{fontSize:16}}>Status : </Text>
                    <Text style={{color:colors.green,fontSize:16}}> Mengirim</Text>
                </View>
            </View>
            <TouchableOpacity style={{flexDirection:'row',
            position:'absolute',
            borderRadius:20,
            backgroundColor:colors.white,
            marginTop: 440,
            marginLeft: '55%',
            width: 60,
            height:40,
            elevation: 10,
            zIndex: 1,}}
            onPress={() => navigation.navigate('Callc')}>
            <Ionicons name='call' size={30} color={colors.green} style={{margin:5}} />
            <Text style={{marginTop:10}}>Call Center</Text>
            </TouchableOpacity>
            <View style={{ 
            position:'absolute',
            borderRadius:40,
            backgroundColor:colors.white,
            alignSelf:'flex-end',
            marginTop: 440,
            width:40,
            height:40,
            elevation: 20,
            zIndex: 1,
            }}>
                <MaterialCommunityIcons name='target' size={30} color={colors.green} style={{margin:5}} />
            </View>
            {/* component bottom */}
            <View
            style={{
            // flex:1,
            width:'100%',
            height:'65%'
            }}
            />
            <View style={{
            // position:'absolute',
            backgroundColor:colors.white,
            width:'100%',
            height:300,
            // marginTop:'125%',
            // alignSelf:'flex-end',
            borderTopLeftRadius:20,
            borderTopRightRadius:20,
            elevation: 10,
            zIndex: 1,
            padding:20}}>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>No. Surat Jalan</Text>
                <Text style={{flex:1}}> : </Text>
                <Text style={{flex:2,textAlign:'right',color:colors.darkblue}}>#1234567890</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Nama Pelanggan</Text>
                <Text style={{flex:1}}> : </Text>
                <Text style={{flex:2,textAlign:'right',color:colors.black}}>Yth. Bapak Kevin Sudrajat</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Tanggal Kirim</Text>
                <Text style={{flex:1}}> : </Text>
                <Text style={{flex:2,textAlign:'right',color:colors.black}}>23 Juli 2020</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:3}}>
                <Text style={{flex:2}}>Jam Kirim</Text>
                <Text style={{flex:1}}> : </Text>
                <Text style={{flex:2,textAlign:'right',color:colors.black}}>07 : 30</Text>
            </View>
            <View style={{flexDirection:'row',marginVertical:15}}>
                <Entypo name='location-pin' size={30} color={colors.green} />
                <View style={{flexDirection:'column'}}>
                <Text style={{color:colors.green}}>Lokasi Penerima</Text>
                <Text>Pabrik Sukabumi</Text>
                </View>
            </View>
            <Button title='Tiba Di Tujuan' buttonStyle={{backgroundColor:colors.darkblue}} 
                onPress={() => {
                setModalVisible(true);
            }}/>
            </View>
            {/* component bottom */}
            {renderModal()}
            {renderModalReject()}
        </SafeAreaView>
    )
}

export default Maps

const styles = StyleSheet.create({})
