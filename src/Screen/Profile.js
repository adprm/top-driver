import React,{useState} from 'react'
import { StyleSheet, Text, View, StatusBar, Image,TouchableOpacity,ScrollView } from 'react-native'
import styleses from '../Style/Styleses';
import colors from '../Style/Colors';
import { Button,Input  } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Fontisto from 'react-native-vector-icons/Fontisto';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

const Profile = ({navigation}) => {
    const [isShow, setIsShow] = useState(true)

    const Pressshow = () => {
        setIsShow(!isShow)
    }

    return (
        <View style={styleses.wrap}>
            <StatusBar translucent backgroundColor="transparent"/>
            <View style={{height:'20%'}}>
                <Image source={require('../Assets/top-background.png')} style={{width:'100%',height:280}} />
                {/* Profile Top*/}
                <View style={{position:'absolute',marginTop:35,marginLeft:10}}>
                <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <AntDesign name={'arrowleft'} size={30} color={colors.white} />
                </TouchableOpacity>
                </View>
                <View style={{flexDirection:'column',position:'absolute',alignSelf:'center',marginTop:30}}>
                    <Text style={[styles.font,styles.title]}>Profile</Text>
                    <Image source={require('../Assets/Person.png')} style={{width:100,height:100,borderRadius:100,margin:5,alignSelf:'center'}}/>
                    <Text style={[styles.font,styles.title]}>Ade Rohayat Satria</Text>
                </View>
                {/* Profile Top*/}
            </View>
            <View style={{position:'absolute',backgroundColor:colors.white,width:'100%',height:'80%',marginTop:250,borderTopLeftRadius:30,borderTopRightRadius:30}}>

            {/* Form Profile */}
            <ScrollView>
            <View style={{paddingTop:15,paddingHorizontal:10,paddingBottom:120}}>
            <Input
            placeholder='Input No KTP'
            label='No. KTP'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input No SIM'
            label='No. SIM'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input Jenis Kelamin'
            label='Jenis Kelamin'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input Alamat'
            label='Alamat'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input Tempat, Tanggal Lahir'
            label='Tempat, Tanggal Lahir'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input No Telepon'
            label='No Telepon'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input Email'
            label='Email'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            />
            <Input
            placeholder='Input Password'
            label='Password'
            labelStyle={styleses.inputLabel}
            inputStyle={styleses.input}
            secureTextEntry={isShow}
            rightIcon={
                <TouchableOpacity onPress={() => Pressshow()}>
                <Fontisto
                name='eye'
                size={14}
                color={colors.darkblue}
                />
                </TouchableOpacity>
            }
            />
             <Button 
                title="Keluar"
                icon={
                    <SimpleLineIcons
                      name="logout"
                      size={20}
                      color="white"
                      style={{paddingRight:25}}
                    />
                  }
                buttonStyle={{backgroundColor:colors.red,padding:10,paddingRight:'70%',marginHorizontal:5}}
                onPress={() => navigation.navigate('Login') }
                
                />
            </View>
            </ScrollView>
            {/* Form Profile */}
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    font:{
        color:colors.white,
        margin:10
    },
    title:{
        fontSize:20,
        textAlign:'center'
    },
})
