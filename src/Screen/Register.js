import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, StatusBar } from 'react-native'
import { Button, Input } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import styleses from '../Style/Styleses';
import colors from '../Style/Colors';

const Register = ({ navigation }) => {
  const [isShow, setIsShow] = useState(true)

  const Pressshow = () => {
    setIsShow(!isShow)
  }

  return (
    <View style={styleses.wrap}>
      <StatusBar translucent backgroundColor="transparent" />
      <View>
        <Image source={require('../Assets/top-background.png')} style={{ width: '100%', height: 267 }} />
        <Text style={{ position: 'absolute', fontSize: 36, color: colors.white, padding: 20, marginTop: '40%' }}>Sign In</Text>
      </View>
      <View style={{ margin: 15 }}>
        <Text style={styles.title} >Email / Nomor Telepon</Text>
        <View style={styleses.wrapInput}>
          <Input
            placeholder='Email'
            inputContainerStyle={{ borderBottomWidth: 0 }}
            leftIcon={
              <Ionicons
                name='person'
                size={16}
                color={colors.black}
              />
            }
            style={{ padding: 10 }}
          />
        </View>

        <View style={styleses.wrapInput}>
          <Input
            placeholder='Full Name'
            inputContainerStyle={{ borderBottomWidth: 0 }}
            leftIcon={
              <Ionicons
                name='person'
                size={16}
                color={colors.black}
              />
            }
            style={{ padding: 10 }}
          />
        </View>

        <Text style={styles.title}>Kata Sandi</Text>
        <View style={styleses.wrapInput}>
          <Input
            placeholder='Kata Sandi'
            inputContainerStyle={{ borderBottomWidth: 0 }}
            style={{ padding: 10 }}
            secureTextEntry={isShow}
            leftIcon={<Fontisto
              name='locked'
              size={16}
              color={colors.black}
            />
            }
            rightIcon={
              <TouchableOpacity onPress={() => Pressshow()}>
                <Fontisto
                  name='eye'
                  size={16}
                  color={colors.darkblue}
                />
              </TouchableOpacity>
            }
          />
        </View>
        <TouchableOpacity style={{ marginLeft: 240, marginBottom: 30 }}>
          <Text style={{ color: colors.darkblue }}> Lupa Password? </Text>
        </TouchableOpacity>

        <Button
          title="Berikutnya"
          buttonStyle={{ borderRadius: 5, backgroundColor: colors.darkblue, padding: 10 }}
          onPress={() => navigation.navigate('Home')}
        />
      </View>
    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  title: {
    paddingVertical: 10
  },
})
