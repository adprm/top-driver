import React,{useState,useEffect,useRef } from 'react'
import { StyleSheet, Text, View, SafeAreaView, FlatList, Modal,TouchableOpacity, ToastAndroid } from 'react-native'
import { Button } from 'react-native-elements';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';
import moment from 'moment'
import 'moment/locale/id'
import { RNCamera } from 'react-native-camera';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import Feather from 'react-native-vector-icons/Feather';

const DATA = [
    {
        id : 1,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 2,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 3,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 4,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 5,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 6,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
    {
        id : 7,
        tgl : 'Rabu 22 Juli',
        masuk : '08:00',
        pulang : '17:00',
    },
]

const Item = ({data}) => {
        
    return (
    <View style={{marginHorizontal:20,marginVertical:5}}>
    <View style={{
        width:'100%',
        height: 50
        }}>

    <View style={{flexDirection:'column'}}>
    {/* data */}
    <View style={{flexDirection: 'row',justifyContent: 'space-between',marginVertical:5}}>
    <Text style={{marginTop:10}}>{data.tgl}</Text>
    <Text style={{color:colors.green,marginTop:10}}>Masuk : </Text>
    <Button title={data.masuk} buttonStyle={{backgroundColor:colors.green}}/>
    <Text style={{color:colors.darkblue,marginTop:10}}>Pulang : </Text>
    <Button title={data.pulang} buttonStyle={{backgroundColor:colors.darkblue}}/>
    </View>
    {/* data */}
    </View>
    <View
    style={{
        borderBottomColor: colors.grey,
        borderBottomWidth: 1,
    }}
    />

    </View>
    </View>
    );
}

const renderItem = ({ item }) => (
    <Item data={item} />
  );

const index = () => {
    const [modalVisible, setModalVisible] = useState(false);
    const [modalname, setModalName] = useState('');
    const [currentDate, setCurrentDate] = useState(new Date());

    const camera = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [editable, setEditable] = useState(false)
    const [type, setType] = useState('front')
    const [photo, setPhoto] = useState(null)
    const [token, setToken] = useState('')

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        // const option = {quality: 0.5, base64 : true};
        // if(camera){
        //     const data = await camera.current.takePictureAsync(option);
        //     setPhoto(data)
            setIsVisible(false)
            setModalVisible(!modalVisible)
            ToastAndroid.show("Foto sudah terkirim!", ToastAndroid.SHORT)
        // }
    }


    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1, }}
                        type={type}
                        ref={camera}
                    >
                        {/* <View style={{
                             padding:25,
                             width:80,
                             height:80,
                             borderRadius:80,
                             backgroundColor:colors.blue
                        }}>
                            <TouchableOpacity style={{}} onPress={() => toggleCamera()}>
                                <MaterialCommunityIcons name={'rotate-3d-variant'} size={30} color={colors.white}/>
                            </TouchableOpacity>
                        </View> */}
                          <View style={{
                              width:300,
                              height:'87%',
                            //   borderRadius:80,
                            //   borderColor:'black',
                            //   borderWidth:3,
                            //   borderColor: colors.black,
                              marginHorizontal: 50
                        }} />
                        <View style={{
                            padding:25,
                            width:80,
                            height:80,
                            borderRadius:80,
                            backgroundColor:'black',
                            alignSelf:'center',
                        }} >
                            <TouchableOpacity style={{}} onPress={() => takePicture()}>
                                <Feather name={'camera'} size={30} color={colors.white} />
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    useEffect(() => {
        setTimeout(() =>{
            setCurrentDate(new Date())
        },1000)
      }, [currentDate]);      
      
    
    const renderModal = () => {

        return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            }}>
            <View style={styleses.centeredView}>
            <View style={styleses.modalView}>
                <Text style={[styleses.modalText,{color:colors.darkblue}]}>Absen {modalname}</Text>
                <Text style={{marginTop:10}}>Ambil foto selfie untuk memperoses </Text>
                <Text style={{marginBottom:10}}>absen Silahkan buka kamera</Text>
                <View style={{flexDirection:'row',margin:20}}>
                <Button title="Kembali" type="outline" buttonStyle={{marginRight:10,width:130}} titleStyle={{color:'black'}}
                onPress={() => {
                    setModalVisible(!modalVisible);
                }}/>
                <Button title="Buka Kamera" onPress={() => setIsVisible(true)} buttonStyle={{width:130,backgroundColor:colors.darkblue}}/>
                </View>
            </View>
            </View>
        </Modal>
        )
    }

    return (
        <SafeAreaView style={[styleses.wrap,{backgroundColor:colors.white}]}>
            <View style={{alignItems:'center',flexDirection:'column', justifyContent: 'space-around',flex:1,margin:20}}>
                <Text> Hari ini : {moment(currentDate).format('dddd, DD MMMM YYYY')} </Text>
                <Text style={{fontSize:36,fontWeight:'bold'}}> {moment(currentDate).format(' hh : mm ')} </Text>
                <Text> WIB </Text>
                <Button buttonStyle={{backgroundColor:colors.darkblue}} title='Absen Masuk' onPress={() => {
                    setModalVisible(true)
                    setModalName('Masuk')
                }}/>
                <Button buttonStyle={{backgroundColor:colors.darkblue}} title='Absen Pulang' onPress={() => {
                    setModalVisible(true)
                    setModalName('Pulang')
                }}/>
            </View>
            <View style={{flex:2}}>
            <Text style={{fontSize: 16,marginHorizontal:20,marginVertical:10}}>Riwayat Absensi</Text>

            <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />

            </View>
            {renderModal()}
            {renderCamera()}
        </SafeAreaView>
    )
}

export default index

const styles = StyleSheet.create({})
