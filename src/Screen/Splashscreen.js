import React, { useState, useEffect, useRef } from 'react'
import { View, Text, StyleSheet, StatusBar, Animated, Dimensions, SafeAreaView,ImageBackground,Image } from 'react-native'

const height = Dimensions.get('window').height

const Splashscreens = () => {

    const fadeIn = useRef(new Animated.Value(0)).current

    useEffect(() => {
        Animated.timing(fadeIn, {
            toValue: 1,
            duration: 3000,
            useNativeDriver: false
        }).start()
    }, [fadeIn])

    const transformY = fadeIn.interpolate({
        inputRange: [0, 0],
        outputRange: [height, height / 50]
    })
    
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar translucent backgroundColor="transparent"/>
                <Animated.View style={[styles.quotesContainer, { opacity: fadeIn, transform: [{ translateY: transformY }] }]}>
                    <Image source={require('../Assets/top-driver.png')} 
                    style={{width:160,height:160}}  />
                </Animated.View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DCDCDC'
    },
    quotesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'center'
    },
    quotes: {
        fontSize: 14,
        color: 'white'
    },
})

export default Splashscreens