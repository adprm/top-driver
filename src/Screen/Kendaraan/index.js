import React,{useState} from 'react'
import { StyleSheet, Text, View,Image,TouchableOpacity,StatusBar,SafeAreaView } from 'react-native'
import { Button,Input  } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styleses from '../../Style/Styleses';
import colors from '../../Style/Colors';
import DatePicker from 'react-native-datepicker'
import {Picker} from '@react-native-picker/picker';
import { RadioButton } from 'react-native-paper';
// import { Radio } from 'native-base';


const index = () => {
    const [selectedValue, setSelectedValue] = useState("");
    const [date, setDate] = useState(new Date())
    const [checkedBan, setCheckedBan] = useState('first');
    const [checkedOli, setCheckedOli] = useState('first');
    const [checkedBensin, setCheckedBensin] = useState('second');
    const [checkedElektrik, setCheckedElektrik] = useState('first');
    const [checkedRem, setCheckedRem] = useState('first');
    const [checkedLain, setCheckedLain] = useState('first');

    return (
        <SafeAreaView style={[styleses.wrap,{backgroundColor:colors.white}]}>
            <StatusBar translucent backgroundColor="transparent"/>
            <View style={{margin:20}}>
                <View style={styleses.wrapInput}>
                    {/* <Text style={{fontSize:16}}>Tanggal :</Text> */}
                <View style={{flexDirection:'row',flex:1}}>
                <Text style={{fontSize:16,margin:10}}>Tanggal : </Text>
                <DatePicker
                    style={{width:'70%',marginTop:5}}
                    date={date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="2020-01-01"
                    maxDate="2050-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    // iconComponent={<MaterialIcons name={'calendar-today'} size={20} color={colors.grey}/>}

                    customStyles={{
                    dateInput: {
                        borderWidth:0,
                    },
                    // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => setDate(date)}
                />
                </View>
                </View>

                <View style={styleses.wrapInput}>
                    <View style={{flexDirection:"row"}}> 
                    <Text style={{fontSize:16,marginHorizontal:10,marginTop:14}}>No. Polisi : </Text>
                    {/* <View style={{backgroundColor:'yellow'}}> */}
                        <Picker
                        style={{width:'55%',marginLeft:'13%'}}
                        selectedValue={selectedValue}
                        onValueChange={(itemValue, itemIndex) => {setSelectedValue(itemValue)}}>
                        <Picker.Item label="" value="" />
                        <Picker.Item label="F 2345 BA" value="F 2345 BA" />
                        <Picker.Item label="F 2345 BA" value="F 2345 BA" />
                        <Picker.Item label="B 3421 BC" value="B 3421 BC" />
                        <Picker.Item label="C 3243 ZX" value="C 3243 ZX" />
                        </Picker>
                        {/* </View> */}
                    </View>
                </View>

                <View style={styleses.wrapInput}>
                <Input
                inputContainerStyle={{borderBottomWidth:0}}
                style={{padding:10}}
                leftIcon={ <Text style={{fontSize:16}}>Jenis Kendaraan : </Text> }
                />
                </View>

                <View style={{
                    backgroundColor: colors.grey,
                    borderRadius:5,
                    height:300,
                    width:'100%',
                    marginVertical:10,
                    paddingVertical:15,
                    paddingLeft:10,
                    paddingRight:30
                }}>
                {/* Ban */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Ban</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedBan === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedBan('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedBan === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedBan('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                {/* Oli */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Oli</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedOli === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedOli('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedOli === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedOli('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                {/* Bensin */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Bensin</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedBensin === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedBensin('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedBensin === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedBensin('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                {/* Elektrik */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Elektrik</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedElektrik === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedElektrik('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedElektrik === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedElektrik('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                {/* Rem */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Rem</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedRem === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedRem('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedRem === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedRem('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                {/* Lain */}
                <View style={{flexDirection:'row',marginVertical:5}}>
                    <Text style={{flex:1,marginTop:7}}>Lain - Lain</Text>
                    <Text style={{flex:1,marginTop:7}}>:</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="first"
                        status={ checkedLain === 'first' ? 'checked' : 'unchecked' }
                        color={colors.blue}
                        onPress={() => setCheckedLain('first')}
                    />
                    <Text style={{marginTop:7}}>OK</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <RadioButton
                        value="second"
                        status={ checkedLain === 'second' ? 'checked' : 'unchecked' }
                        color={'red'}
                        onPress={() => setCheckedLain('second')}
                    />
                    <Text style={{marginTop:7}}>Tidak OK</Text>
                    </View>
                </View>
                    
                    
                </View>

                <Button
                title="Simpan"
                buttonStyle={{marginTop:20,borderRadius:5,backgroundColor:colors.green}}
                onPress={() => {}}
                />
            </View>
        </SafeAreaView>
    )
}

export default index

const styles = StyleSheet.create({
    title:{
        paddingVertical:10
    },
})

