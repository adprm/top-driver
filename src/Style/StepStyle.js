import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import colors from './Colors';

    const StepStyle = {
        stepIndicatorSize: 20,
        currentStepIndicatorSize: 20,
        separatorStrokeWidth: 4,
        currentStepStrokeWidth: 4,
        stepStrokeCurrentColor: 'rgba(27,91,202,0.5)',
        stepStrokeWidth: 4,
        stepStrokeFinishedColor: 'rgba(36,177,59,0.5)',
        stepStrokeUnFinishedColor: 'red',
        separatorFinishedColor: colors.grey,
        separatorUnFinishedColor: colors.grey,
        stepIndicatorFinishedColor: colors.green,
        stepIndicatorUnFinishedColor: '#ffffff',
        stepIndicatorCurrentColor: colors.darkblue,
        stepIndicatorLabelFontSize: 13,
        currentStepIndicatorLabelFontSize: 13,
        stepIndicatorLabelCurrentColor: colors.darkblue,
        stepIndicatorLabelFinishedColor: colors.green,
        stepIndicatorLabelUnFinishedColor: 'red',
        labelColor: colors.green,
        //   labelSize: 15,
        currentStepLabelColor: colors.darkblue,
    }

export default StepStyle
