import React from 'react'
import { StyleSheet } from 'react-native'
import colors from './Colors';

const styleses = StyleSheet.create({
    wrap:{
        flex:1
    },
    wrapInput:{
        backgroundColor: colors.grey,
        borderRadius:5,
        height:50,
        marginVertical:10
    },
    // Modal Terima
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10
      },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 10,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        height:200,
        width:330
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize:21,
        fontWeight:'bold'
      },
    // Modal Terima
  inputLabel:{
      fontSize:13,
      fontWeight:'100',
  },
  input:{
      fontSize:14,
  }
})

export default styleses